from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404

from django.templatetags.static import static

from .models import Post


def check_view(request):
    return render(request, 'check.html')

def index_view(request):
    post = Post.objects.all()
    return render(request, 'index.html', context={'posts': post})

def post_list_view (request):
    posts = Post.objects.all()
    return render(request, 'post_list.html', context= {'posts': posts})

def post_detail_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk = kwargs.get('pk'))
    # try:
    #     post = Post.objects.get(pk=kwargs.get('pk')) #but i can still do simply (pk=pk(from  def))
    # except Post.DoesNotExist as error:
    #     raise Http404
    
    return render(request, 'post_detail.html', 
                  context={'post': post})

def post_create_view(request):

    if request.method == 'GET':
        return render(request, 'post_create.html')
    elif request.method == 'POST':
        title = request.POST.get('title')
        body = request.POST.get('body')
        author = request.POST.get('author')
        errors = dict()

        if not title:
            errors['title']=  'Title should not be empty!'
        elif len(title) > 50:
            errors['title']= 'Title should be less than 50 symbols'

        if not author:
            errors['author']= 'Author should not be empty'
        elif len(author) > 30:
            errors['author']= "Author's name should be less than 30 symbols"

        if len(errors)>0:
            post = Post(title=title, body=body, author = author)
            return render(request, 'post_create.html', context={
                'errors':errors,
                'post':post,
                })
        else:
            new_post = Post.objects.create(
                title = title,
                body = body,
                author = author,
            )
        return redirect ('home_page')


def post_update_view(request, *args, **kwargs):
   post = get_object_or_404(Post, pk = kwargs.get('pk'))
   post.title = request.POST.get('title')
   post.body = request.POST.get('body')
   post.author = request.POST.get('author')

   
   if request.method == 'GET':
       return render (request, 'post_update.html', context={'post':post})
   elif request.method == 'POST':
       errors = dict()
       if not post.title:
           errors['title']= 'Title should not be empty!'
       if not post.author:
           errors['author'] = 'Author should not be empty!'
       if errors:
           return render(request, 'post_update.html', context = {
               'errors':errors,
               'post':post
           }) 
       else:
           post.save()
           return redirect ('post_detail.html', pk = post.pk)

def post_delete_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk = kwargs.get('pk'))
    if request.method == 'GET':
        return render (request, 'post_delete.html', context={'post':post})
    elif request.method == 'POST':
        post.delete()
        return redirect ('home_page')
# Create your views here.