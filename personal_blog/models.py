from django.db import models

class Post(models.Model):
    title = models.CharField(max_length = 200, null = False, blank = False, verbose_name= 'Zagolovok')
    body = models.TextField(max_length = 2000, null= True, blank=True, verbose_name = 'Telo')
    author = models.CharField(max_length = 100, null = False, blank = False, verbose_name = 'author')
    created_at = models.DateTimeField(auto_now_add = True, verbose_name = 'data sozdaniya')
    updated_at = models.DateTimeField(auto_now=True, verbose_name = 'data redact')


    def __str__(self) -> str:
        return f'#{self.pk} - {self.title}'
    


# Create your models here.
 